package com.hubino.service.impl;

import com.hubino.model.user.User;
import com.hubino.repo.IBaseRepository;
import com.hubino.repo.IUserRepository;
import com.hubino.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl extends BaseServiceImpl<User, IBaseRepository<User>> implements IUserService {

    @Autowired
    private IUserRepository userRepository;

    @PostConstruct
    private void initBean(){
        this.baseRepository=userRepository;
    }

    @Override
    public User getUserById(int id) {
        return userRepository.getUserById(id);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<User> getAllByNames(List<String> nameList) {
        return userRepository.getAllByNames(nameList);
    }

    @Override
    public List<User> getAllByName(String name) {
        return userRepository.getAllByName(name);
    }

    @Override
    public User getByName(String name) {
        return userRepository.getByName(name);
    }

    @Override
    public List<User> getAllByDepartment_DeptName(String deptName) {
        return userRepository.getAllByDepartment_DeptName(deptName);
    }

    @Override
    public List<User> getAllByRole_RoleName(String roleName) {
        return userRepository.getAllByRole_RoleName(roleName);
    }

    @Override
    public List<User> getAllUsersByCity(String city) {
        return userRepository.getAllUsersByCity(city);
    }

    @Override
    public List<User> getAllUsersByState(String state) {
        return userRepository.getAllUsersByState(state);
    }

    @Override
    public List<User> getAllUsersByCountry(String country) {
        return userRepository.getAllUsersByCountry(country);
    }

    @Override
    public User saveOrUpdate(User entity) {
        return userRepository.save(entity);
    }

}
