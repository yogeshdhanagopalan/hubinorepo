package com.hubino.service.impl;

import com.hubino.model.base.BaseEntity;
import com.hubino.model.user.Role;
import com.hubino.repo.IBaseRepository;
import com.hubino.repo.IRoleRepository;
import com.hubino.service.IBaseService;
import com.hubino.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

@Service
@Transactional
public class RoleServiceImpl extends BaseServiceImpl<Role, IBaseRepository<Role>> implements IRoleService {

    @Autowired
    private IRoleRepository roleRepository;

    @PostConstruct
    private void initBean(){this.baseRepository=roleRepository;}

    @Override
    public Role getById(int id) {
        return roleRepository.getById(id);
    }

    @Override
    public Role saveOrUpdate(Role entity) {
        return roleRepository.save(entity);
    }
}
