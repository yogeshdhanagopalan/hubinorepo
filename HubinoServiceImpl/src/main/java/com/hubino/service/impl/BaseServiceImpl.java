package com.hubino.service.impl;

import com.hubino.model.base.BaseEntity;
import com.hubino.repo.IBaseRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public abstract class BaseServiceImpl<E extends BaseEntity, R extends IBaseRepository> {

    protected R baseRepository;

    @Transactional
    public E findOne(int id) {

        return (E) baseRepository.getOne(id);
    }



    @Transactional(readOnly = true)
    public List< E > findAll() {
        return baseRepository.findAll();
    }

    @Transactional
    public void create(E entity) {
        baseRepository.save(entity);
    }

    @Transactional
    public E update(E entity) {
        return (E)baseRepository.save(entity);
    }

    @Transactional
    public void delete(E entity) {
        baseRepository.delete(entity);
    }

    @Transactional
    public void deleteById(int entityId) {
        baseRepository.deleteById(entityId);
    }



}
