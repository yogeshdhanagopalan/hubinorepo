package com.hubino.service.impl;

import com.hubino.model.user.Address;
import com.hubino.repo.IAddressRepository;
import com.hubino.repo.IBaseRepository;
import com.hubino.service.IAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
@Transactional
public class AddressServiceImpl extends BaseServiceImpl<Address, IBaseRepository> implements IAddressService {

    @Autowired
    private IAddressRepository addressRepository;

    @PostConstruct
    private void initRepo(){
        this.baseRepository = addressRepository;
    }


    @Override
    public List<Address> getAddressByUserId(int userId) {
        return addressRepository.getAddressByUserId(userId);
    }

    @Override
    public Address getAddressById(int id) {
        return addressRepository.getAddressById(id);
    }

    @Override
    public Address getByIdAndUserId(int id, int userId) {
        return addressRepository.getByIdAndUserId(id, userId);
    }

    @Override
    public Address saveOrUpdate(Address entity) {
        return addressRepository.save(entity);
    }
}
