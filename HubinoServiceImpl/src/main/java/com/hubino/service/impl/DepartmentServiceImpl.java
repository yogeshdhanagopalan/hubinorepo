package com.hubino.service.impl;

import com.hubino.model.user.Department;
import com.hubino.repo.IBaseRepository;
import com.hubino.repo.IDepartmentRepository;
import com.hubino.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
@Transactional
public class DepartmentServiceImpl extends BaseServiceImpl<Department, IBaseRepository<Department>> implements IDepartmentService {

    @Autowired
    private IDepartmentRepository departmentRepository;

    @PostConstruct
    private void initBean(){
        this.baseRepository=departmentRepository;
    }

    @Override
    public Department getById(int id) {
        return departmentRepository.getDepartmentById(id);
    }

    @Override
    public Department saveOrUpdate(Department entity) {
        return departmentRepository.save(entity);
    }
}
