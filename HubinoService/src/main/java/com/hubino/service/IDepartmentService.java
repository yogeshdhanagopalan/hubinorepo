package com.hubino.service;

import com.hubino.model.user.Department;

import java.util.List;

public interface IDepartmentService extends IBaseService<Department> {

    Department getById(int id);
}
