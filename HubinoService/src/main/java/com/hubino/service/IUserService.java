package com.hubino.service;

import com.hubino.model.user.User;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IUserService extends IBaseService<User> {

    User getUserById(int id);

    List<User> getAllByNames(List<String> nameList);

    List<User> getAllByName(String name);

    User getByName(String name);

    User findByEmail(String email);

    List<User> getAllByDepartment_DeptName(String deptName);

    List<User> getAllByRole_RoleName(String roleName);

    List<User> getAllUsersByCity(String city);

    List<User> getAllUsersByState(String state);

    List<User> getAllUsersByCountry(String country);

}
