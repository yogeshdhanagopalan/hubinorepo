package com.hubino.service;

import com.hubino.model.user.Address;

import java.util.List;

public interface IAddressService extends IBaseService<Address> {

    List<Address> getAddressByUserId(int userId);

    Address getAddressById(int id);

    Address getByIdAndUserId(int id, int userId);

    //List<Address> saveAll(List<Address> addressList);
}
