package com.hubino.service;

import com.hubino.model.base.BaseEntity;

import java.io.Serializable;
import java.util.List;

public interface IBaseService<T extends BaseEntity> extends Serializable{

    T findOne(int id);
    List<T> findAll();

    void create( T entity );
    T saveOrUpdate( T entity );
    void delete( T entity );
    void deleteById( int entityId);


}
