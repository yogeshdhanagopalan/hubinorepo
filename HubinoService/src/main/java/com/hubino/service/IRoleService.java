package com.hubino.service;

import com.hubino.model.user.Role;

public interface IRoleService extends IBaseService<Role> {

    Role getById(int id);

}
