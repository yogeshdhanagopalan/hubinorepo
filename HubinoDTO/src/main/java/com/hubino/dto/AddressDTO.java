package com.hubino.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AddressDTO {

    @JsonProperty("city")
    private String city;
    @JsonProperty("region_name")
    private String regionName;
    @JsonProperty("country_name")
    private String countryName;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @Override
    public String toString() {
        return "AddressDTO{" +
                "city='" + city + '\'' +
                ", region_name='" + regionName + '\'' +
                ", country_name='" + countryName + '\'' +
                '}';
    }
}
