package com.hubino.dto;

public class LoginResponse {

    private final String responseToken;

    public LoginResponse(String responseToken) {
        this.responseToken = responseToken;
    }


    public String getResponseToken() {
        return responseToken;
    }
}
