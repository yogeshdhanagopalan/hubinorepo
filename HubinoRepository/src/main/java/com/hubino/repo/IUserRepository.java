package com.hubino.repo;

import com.hubino.model.user.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IUserRepository extends IBaseRepository<User> {


    //using native query

    @Query(value = "SELECT * FROM USER WHERE ID = :id", nativeQuery = true)
    User getUserById(@Param("id") int id);

    User findByEmail(@Param("email")String email);

    //User details by name
    @Query(value = "select u from User u where u.name in(:names)")
    List<User> getAllByNames(@Param("names") List<String> nameList);

    //All User details by name
    List<User> getAllByName(@Param("name") String name);

    //User by name
    User getByName(@Param("name")String name);

    //User details by DeptName
    List<User> getAllByDepartment_DeptName(@Param("deptName") String deptName);

    //User details by roleName
    List<User> getAllByRole_RoleName(@Param("roleName")String roleName);

    //User details by city, state, country

    @Query(value = "select u from Address a join a.user u where a.city=:city")
    List<User> getAllUsersByCity(@Param("city")String city);

    @Query(value = "select u from Address a join a.user u where a.state=:state")
    List<User> getAllUsersByState(@Param("state")String state);

    @Query(value = "select u from Address a join a.user u where a.country=:country")
    List<User> getAllUsersByCountry(@Param("country")String country);

}
