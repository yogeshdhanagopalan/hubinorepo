package com.hubino.repo;

import com.hubino.model.user.Department;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IDepartmentRepository extends IBaseRepository<Department> {

    //Using @Query
    @Query("select d from Department d where id=:id")
    Department getDepartmentById(@Param("id") int id);
}
