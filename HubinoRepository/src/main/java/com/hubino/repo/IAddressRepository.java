package com.hubino.repo;

import com.hubino.model.user.Address;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IAddressRepository extends IBaseRepository<Address> {


    @Query("select a from Address a where a.user.id=:userId")
    List<Address> getAddressByUserId(@Param("userId")int userId);

    Address getAddressById(@Param("id")int id);

    Address getByIdAndUserId(@Param("id")int id, @Param("userId")int userId);

}
