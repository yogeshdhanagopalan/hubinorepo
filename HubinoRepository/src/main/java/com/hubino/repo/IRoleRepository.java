package com.hubino.repo;

import com.hubino.model.user.Role;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IRoleRepository extends IBaseRepository<Role> {

    //jpa api
    Role getById(@Param("id")int id);

}
