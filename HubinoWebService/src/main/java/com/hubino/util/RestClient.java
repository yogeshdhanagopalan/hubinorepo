package com.hubino.util;

import com.hubino.dto.AddressDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestClient {

    public static final String BASE_URL = "http://api.ipstack.com/";
    public static final String ACCESS_KEY = "626053333c5203dcd83c391d63485afb";

    @Autowired
    RestTemplate restTemplate;


    public AddressDTO getLocationDetails(String ipAddress){

        String fullUrl = BASE_URL + ipAddress + "?access_key=" + ACCESS_KEY;

        System.out.println("Full URL......" + fullUrl);

        ResponseEntity<AddressDTO> responseEntity = restTemplate.getForEntity(fullUrl, AddressDTO.class);

        return responseEntity.getBody();
    }




}
