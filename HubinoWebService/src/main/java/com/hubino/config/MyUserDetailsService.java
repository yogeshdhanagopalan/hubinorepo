package com.hubino.config;

import com.hubino.model.user.User;
import com.hubino.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private IUserService userService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        //new org.springframework.security.core.userdetails.User("foo", "foo", new ArrayList<>());

        User user = userService.findByEmail(email);
        String userEmail = user.getEmail();
        String pwd = user.getPassword();


        return org.springframework.security.core.userdetails.User.withUsername(userEmail)
                .password(pwd)
                .authorities(new ArrayList<>())
                .build();
    }
}
