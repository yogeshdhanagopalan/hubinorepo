package com.hubino.api;


import com.hubino.config.JwtUtil;
import com.hubino.config.MyUserDetailsService;
import com.hubino.dto.LoginRequest;
import com.hubino.dto.LoginResponse;
import com.hubino.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/login")
public class LoginController {

    @Autowired
    private MyUserDetailsService userDetailsService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserService userService;

    @Autowired
    private JwtUtil jwtUtil;


    @PostMapping(value = "/authenticate")
    private ResponseEntity<LoginResponse> authenticateUser(@RequestBody()LoginRequest loginRequest) throws Exception{ //payload in the post body
        String email = loginRequest.getEmail();
        String password = loginRequest.getPassword();

        com.hubino.model.user.User user = userService.findByEmail(email);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e){
            System.out.println("Invalid credentials");
            System.out.println(e.getMessage());
        }



        final UserDetails userDetails = userDetailsService.loadUserByUsername(email);



        final String token = jwtUtil.generateToken(userDetails);
        return ResponseEntity.ok(new LoginResponse(token));
    }


}
