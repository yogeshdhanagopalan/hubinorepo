package com.hubino.api;

import com.hubino.config.JwtRequestFilter;
import com.hubino.config.JwtUtil;
import com.hubino.dto.AddressDTO;
import com.hubino.model.user.Address;
import com.hubino.model.user.Department;
import com.hubino.model.user.Role;
import com.hubino.model.user.User;
import com.hubino.service.IDepartmentService;
import com.hubino.service.IRoleService;
import com.hubino.service.IUserService;
import com.hubino.util.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "/user")
public class UserController {

    private static final String ADMIN = "admin";
    private static final String ECE = "ece";

    @Autowired
    private IUserService userService;

    @Autowired
    private IRoleService roleService;

    @Autowired
    private IDepartmentService departmentService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private JwtRequestFilter requestFilter;

    @Autowired
    private JwtUtil jwtUtil;


    @Autowired
    private RestClient restClient;

    @GetMapping(value = "/getById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private User getByUserId(@PathVariable("id")int id){
        return userService.getUserById(id);
    }


    @GetMapping(value = "/getAllByNames", produces = MediaType.APPLICATION_JSON_VALUE)
    private List<User> getAllByNames(@RequestBody()List<String> names){
        return userService.getAllByNames(names);
    }


    @GetMapping(value = "/getByName/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    private User getByName(@PathVariable("name")String name){
        return userService.getByName(name);
    }


    @GetMapping(value = "/getAllByDept/{deptName}", produces = MediaType.APPLICATION_JSON_VALUE)
    private List<User> getAllByDept(@PathVariable("deptName")String deptName){
        return userService.getAllByDepartment_DeptName(deptName);
    }


    @GetMapping(value = "/getAllByRole/{roleName}", produces = MediaType.APPLICATION_JSON_VALUE)
    private List<User> getAllByRole(@PathVariable("roleName")String roleName){
        return userService.getAllByRole_RoleName(roleName);
    }


    @GetMapping(value = "/getAllByCity/{city}", produces = MediaType.APPLICATION_JSON_VALUE)
    private List<User> getAllByCity(@PathVariable("city")String city){
        return userService.getAllUsersByCity(city);
    }

    @GetMapping(value = "/getAllByState/{state}", produces = MediaType.APPLICATION_JSON_VALUE)
    private List<User> getAllByState(@PathVariable("state")String state){
        return userService.getAllUsersByState(state);
    }

    @GetMapping(value = "/getAllByCountry/{country}", produces = MediaType.APPLICATION_JSON_VALUE)
    private List<User> getAllByCountry(@PathVariable("country")String country){
        return userService.getAllUsersByCountry(country);
    }

    @PostMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    private User updateUser(@RequestBody()User user){
        return userService.saveOrUpdate(user);
    }


    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    private User createUser(@RequestBody()User user){
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        if (user.getAddressList().size() > 0){
            user.getAddressList().forEach(address -> {
                if (address.getUser() == null)address.setUser(user);
            });
        }
        return userService.saveOrUpdate(user);
    }

    //Register API - user with name, email, password, role_id, department_id
    @PostMapping(value = "/register/{name}/{email}/{password}/{roleId}/{departmentId}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    private String createUser(@PathVariable("name")String name, @PathVariable("email")String email,
                                    @PathVariable("password")String password, @PathVariable("roleId")int roleId,
                                    @PathVariable("departmentId")int departmentId){

        User user = new User();
        Role role = roleService.getById(roleId);
        Department department = departmentService.getById(departmentId);

        user.setName(name);
        user.setEmail(email);
        String encodedPwd = bCryptPasswordEncoder.encode(password);
        user.setPassword(encodedPwd);//Todo password need to be encrypted
        user.setRole(role);
        user.setDepartment(department);

        //Adding default Address
        List<Address> addressList = new ArrayList<>();
        Address address = new Address();
        address.setCity("Chennai");
        address.setState("Tamil Nadu");
        address.setCountry("India");
        address.setUser(user);

        user.setAddressList(addressList);

        if (isValidUser(user)) {
            userService.saveOrUpdate(user);
            return "User created Successfully";
        }

        return "Whoops! There is something wrong in saving the data";
    }

    @PostMapping(value = "update/{userId}/{ipAddress}", produces = MediaType.APPLICATION_JSON_VALUE)
    private String updateUser(@PathVariable("userId")int userId, @PathVariable("ipAddress")String ipAddress){

        //Todo get the jwt token and get the user by token
        String token = requestFilter.getJwtToken();
        String email = jwtUtil.extractUsername(token);

        User authenticatedUser = userService.findByEmail(email);


        User user = userService.getUserById(userId);
        boolean isUserBelongsToSameDept = authenticatedUser.getDepartment().getDeptName()
                .equalsIgnoreCase(user.getDepartment().getDeptName());

        //boolean isAdmin = user.getRole().getRoleName().equalsIgnoreCase(ADMIN);



        if (isUserBelongsToSameDept){
            AddressDTO addressDTO = restClient.getLocationDetails(ipAddress);
            List<Address> addressList = user.getAddressList();

            addressList.forEach(address -> {
                address.setCountry(addressDTO.getCountryName());
                address.setState(addressDTO.getRegionName());
                address.setCity(addressDTO.getCity());
                address.setLastUpdatedOn(new Date());
            });
            user.setLastUpdatedOn(new Date());
            userService.saveOrUpdate(user);
            return "User details has been updated successfully";
        }

        return "Sorry! you must be admin user in order to update user details";
    }

    @PostMapping(value = "update/{userId}/{addressId}/{ipAddress}", produces = MediaType.APPLICATION_JSON_VALUE)
    private String updateUser(@PathVariable("userId")int userId,@PathVariable("addressId")int addressId ,
                              @PathVariable("ipAddress")String ipAddress){

        AddressDTO addressDTO = restClient.getLocationDetails(ipAddress);
        User user = userService.getUserById(userId);

        boolean isAdmin = user.getRole().getRoleName().equalsIgnoreCase(ADMIN);
        if (isAdmin){
            List<Address> addressList = user.getAddressList();

            addressList.forEach(address -> {
                if (address.getId().equals(addressId)){
                    address.setCity(addressDTO.getCity());
                    address.setState(addressDTO.getRegionName());
                    address.setCountry(addressDTO.getCountryName());
                }
            });
            user.setLastUpdatedOn(new Date());
            userService.saveOrUpdate(user);
            return "User details has been updated successfully";
        }


        return "Sorry! you must be admin user in order to update user details";
    }


    private boolean isValidUser(User user){

        return user != null && user.getRole() != null && user.getDepartment() != null;

    }

}
