package com.hubino.api;

import com.hubino.model.user.Department;
import com.hubino.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/department")
public class DepartmentController {

    @Autowired
    private IDepartmentService departmentService;



    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    private String save(@RequestBody()Department department){
        departmentService.saveOrUpdate(department);
        return "Department added successfully";
    }




}
