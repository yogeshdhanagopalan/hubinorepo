package com.hubino.api;

import com.hubino.model.user.Role;
import com.hubino.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/role")
public class RoleController {

    @Autowired
    private IRoleService roleService;

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    private String saveRole(@RequestBody()Role role){
        roleService.saveOrUpdate(role);
        return "Hello there! new role has been added successfully with id " + role.getId();
    }

    @GetMapping(value = "/getById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Role getById(@PathVariable("id") int id){
        return roleService.getById(id);
    }

}
