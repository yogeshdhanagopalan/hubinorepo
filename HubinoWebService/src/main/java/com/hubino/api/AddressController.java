package com.hubino.api;

import com.hubino.dto.AddressDTO;
import com.hubino.model.user.Address;
import com.hubino.model.user.User;
import com.hubino.service.IAddressService;
import com.hubino.service.IUserService;
import com.hubino.util.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping(path = "/address")
public class AddressController {

    @Autowired
    IAddressService addressService;

    @Autowired
    IUserService userService;


    @PostMapping(value = "add/{userId}/{city}/{state}/{country}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    private String addAddress(@PathVariable("userId")int userId, @PathVariable("city")String city,
                              @PathVariable("state")String state, @PathVariable("country")String country){

        Address address = new Address();
        User user = userService.getUserById(userId);

        if (user != null){
            address.setUser(user);
            address.setCity(city);
            address.setState(state);
            address.setCountry(country);

            addressService.saveOrUpdate(address);
            return "Address added for user id: " + userId;
        }

        return "Whoops! There is something wrong in saving the data";
    }


}
