package com.hubino.model.user;

import com.hubino.model.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DEPARTMENT")
public class Department extends BaseEntity{

    @Column(name = "DEPARTMENT_NAME")
    private String deptName;

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

}
